//
//  main.m
//  MaximumPathSum_I_18
//
//  Created by Andrew Romanov on 13/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <iostream>
#import <string>
#import <sstream>
#import <fstream>
#import <vector>
#import <limits>
#import <map>


/*
 https://projecteuler.net/problem=18
 
 By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.
 
 3*
 7* 4
 2 4* 6
 8 5 9* 3
 
 That is, 3 + 7 + 4 + 9 = 23.
 
 Find the maximum total from top to bottom of the triangle below:
 
 75
 95 64
 17 47 82
 18 35 87 10
 20 04 82 47 65
 19 01 23 75 03 34
 88 02 77 73 07 63 67
 99 65 04 28 06 16 70 92
 41 41 26 56 83 40 80 70 33
 41 48 72 33 47 32 37 16 94 29
 53 71 44 65 25 43 91 52 97 51 14
 70 11 33 28 77 73 17 78 39 68 17 57
 91 71 52 38 17 14 91 43 58 50 27 29 48
 63 66 04 68 89 53 67 30 73 16 69 87 40 31
 04 62 98 27 23 09 70 98 73 93 38 53 60 04 23
 
 NOTE: As there are only 16384 routes, it is possible to solve this problem by trying every route. However, Problem 67, is the same challenge with a triangle containing one-hundred rows; it cannot be solved by brute force, and requires a clever method! ;o)
 
 
 https://projecteuler.net/problem=67
 
 Find the maximum total from top to bottom in triangle.txt (right click and 'Save Link/Target As...'), a 15K text file containing a triangle with one-hundred rows.
 
 NOTE: This is a much more difficult version of Problem 18. It is not possible to try every route to solve this problem, as there are 299 altogether! If you could check one trillion (1012) routes every second it would take over twenty billion years to check them all. There is an efficient algorithm to solve it. ;o)
 */

class UnmutableSubtriangle;

class Triangle
{
public:
	Triangle()
	{}
	
	Triangle(std::istream& stream)
	{
		initWithStream(stream);
	}
	virtual ~Triangle() {}
	
	typedef uint Value_t;
	
	virtual size_t countItemsOnLine(size_t lineNumber) const
	{
		size_t count = lineNumber + 1;
		return count;
	}
	
	virtual  size_t countLines() const
	{
		return _store.size();
	}
	
	virtual  Value_t valueForIndexAtLine(size_t lineNumber, size_t index) const
	{
		const Line_t& line = _store.at(lineNumber);
		Value_t value = line.at(index);
		return value;
	}
	virtual Triangle* subtriangleWithVertexAtLineAndIndex(size_t lineNumber, size_t index) const;
	
	void initWithStream(std::istream& stream)
	{
		_store.clear();
		
		while (!stream.eof())
		{
			Line_t lineData;
			
			std::string line;
			std::getline(stream, line);
			if (line.length() > 0)
			{
				std::stringstream lineStream(line);
				while (!lineStream.eof())
				{
					Value_t value = 0;
					lineStream >> value;
					lineData.push_back(value);
				}
				
				_store.push_back(lineData);
			}
		}
	}
	
private:
	typedef std::vector<Value_t> Line_t;
	typedef std::vector<Line_t> TriangleStore_t;
	
	TriangleStore_t _store;
	
};


class UnmutableSubtriangle : public Triangle
{
public:
	UnmutableSubtriangle(const Triangle* parent, size_t vertexLine, size_t vertexIndex)
	:Triangle(), _parentTriangle(parent), _vertexLine(vertexLine), _vertexIndex(vertexIndex)
	{}
	
	virtual  size_t countLines() const
	{
		size_t countParent = _parentTriangle->countLines();
		size_t count = countParent - _vertexLine;
		return count;
	}
	
	virtual  Value_t valueForIndexAtLine(size_t lineNumber, size_t index) const
	{
		size_t parentLineNumber = lineNumber + _vertexLine;
		size_t parentIndex = index + _vertexIndex;
		Value_t value = _parentTriangle->valueForIndexAtLine(parentLineNumber, parentIndex);
		return value;
	}
	
private:
	const Triangle* _parentTriangle;
	
	size_t _vertexLine;
	size_t _vertexIndex;
	
};


Triangle* Triangle::subtriangleWithVertexAtLineAndIndex(size_t lineNumber, size_t index) const
{
	UnmutableSubtriangle* subtriangle = new UnmutableSubtriangle(this, lineNumber, index);
	return subtriangle;
}


std::ostream& operator << (std::ostream& outStream, const Triangle& triangle)
{
	for (size_t line = 0; line < triangle.countLines(); line++)
	{
		for (size_t index = 0; index < triangle.countItemsOnLine(line); index++)
		{
			outStream << triangle.valueForIndexAtLine(line, index);
			if(index != triangle.countItemsOnLine(line) - 1)
			{
				outStream << "\t";
			}
		}
		
		if(line != triangle.countLines() - 1)
		{
			outStream << "\n";
		}
	}
	
	
	return outStream;
}


Triangle loadTriangleFromFile(const std::string& fileName)
{
	std::ifstream inputFileStream(fileName);
	Triangle triangle(inputFileStream);
	
	return triangle;
}

struct VertexPosition {
	size_t lineNumber;
	size_t index;
};

bool operator < (const VertexPosition& x, const VertexPosition& y)
{
	BOOL less = true;
	if (x.lineNumber != y.lineNumber)
	{
		less = x.lineNumber < y.lineNumber;
	}
	else
	{
		less = x.index < y.index;
	}
	
	return less;
}


class SumCache
{
public:
	SumCache()
	{}
	
	inline bool containsValueForVertex(const VertexPosition& vertex) const
	{
		Key_t key = keyForVertex(vertex);
		Store_t::const_iterator iterator = _cache.find(key);
		
		return iterator != _cache.cend();
	}
	
	inline int64_t valueForVertex(const VertexPosition& vertex) const
	{
		Key_t key = keyForVertex(vertex);
		Store_t::const_iterator iterator = _cache.find(key);
		
		int64_t value = std::numeric_limits<int64_t>::min();
		if (iterator != _cache.cend())
		{
			value = iterator->second;
		}
		
		return value;
	}
	
	inline void setValueForVertex(const VertexPosition& vertex, uint64_t value)
	{
		_cache.insert(Store_t::value_type(keyForVertex(vertex), value));
	}
	
private:
	typedef VertexPosition Key_t;
	typedef std::map<Key_t, int64_t> Store_t;
	
	Key_t keyForVertex(const VertexPosition& vertex) const
	{
		return vertex;
	}
	
private:
	Store_t _cache;
};



bool existNextLine(const Triangle* triangle, size_t lineNumber)
{
	size_t countLines = triangle->countLines();
	bool exist = lineNumber < countLines - 1;
	return exist;
}


bool existLeftForVertex(const Triangle* triangle, const VertexPosition& vertexPosition)
{
	bool exist = existNextLine(triangle, vertexPosition.lineNumber);
	return exist;
}


bool existRightForVertex(const Triangle* triangle, const VertexPosition& vertexPosition)
{
	bool exist = existNextLine(triangle, vertexPosition.lineNumber);
	return exist;
}


VertexPosition leftVertexPositionForVertex(const VertexPosition& vertexPosition)
{
	VertexPosition left = {vertexPosition.lineNumber + 1, vertexPosition.index};
	return left;
}


VertexPosition rightVertexPositionForVertex(const VertexPosition& vertexPosition)
{
	VertexPosition right = {vertexPosition.lineNumber + 1, vertexPosition.index + 1};
	return right;
}

void addRightToSum(const Triangle* triangle, const VertexPosition& vertexPosition, int64_t sum, void(^callback)(int64_t sum));

void addLeftToSum(const Triangle* triangle, const VertexPosition& vertexPosition, int64_t sum, void(^callback)(int64_t sum))
{
	VertexPosition leftVertex = leftVertexPositionForVertex(vertexPosition);
	sum += triangle->valueForIndexAtLine(leftVertex.lineNumber, leftVertex.index);
	
	bool existLeft = existLeftForVertex(triangle, leftVertex);
	bool existRight = existRightForVertex(triangle, leftVertex);
	if (!existLeft && !existRight)
	{
		callback(sum);
	}
	if (existLeft)
	{
		addLeftToSum(triangle, leftVertex, sum, callback);
	}
	if (existRight)
	{
		addRightToSum(triangle, leftVertex, sum, callback);
	}
}


void addRightToSum(const Triangle* triangle, const VertexPosition& vertexPosition, int64_t sum, void(^callback)(int64_t sum))
{
	VertexPosition rightVertex = rightVertexPositionForVertex(vertexPosition);
	sum += triangle->valueForIndexAtLine(rightVertex.lineNumber, rightVertex.index);
	
	bool existLeft = existLeftForVertex(triangle, rightVertex);
	bool existRight = existRightForVertex(triangle, rightVertex);
	if (!existLeft && !existRight)
	{
		callback(sum);
	}
	if (existLeft)
	{
		addLeftToSum(triangle, rightVertex, sum, callback);
	}
	if (existRight)
	{
		addRightToSum(triangle, rightVertex, sum, callback);
	}
}


//calls callback on every leaf;
void sumLinesWithCallback(const Triangle* triangle, void(^callback)(int64_t sum))
{
	VertexPosition startPosition = {UINT64_C(0), UINT64_C(0)};
	int64_t sum = triangle->valueForIndexAtLine(startPosition.lineNumber, startPosition.index);
	bool existLeft = existLeftForVertex(triangle, startPosition);
	bool existRight = existRightForVertex(triangle, startPosition);
	if (!existLeft && !existRight)
	{
		callback(sum);
	}
	if (existLeft)
	{
		addLeftToSum(triangle, startPosition, sum, callback);
	}
	if (existRight)
	{
		addRightToSum(triangle, startPosition, sum, callback);
	}
}


int64_t maxSumForTriangle(const Triangle* triangle)
{
	__block int64_t maxSum = std::numeric_limits<int64_t>::min();
	
	sumLinesWithCallback(triangle, ^(int64_t sum) {
		if (maxSum < sum)
		{
			maxSum = sum;
		}
	});
	
	return maxSum;
}


SumCache cache;

int64_t maxSumFromVertexForTriangle(const Triangle* triangle, const VertexPosition& vertexPosition)
{
	int64_t maxSum = std::numeric_limits<int64_t>::min();
	if (cache.containsValueForVertex(vertexPosition))
	{
		maxSum = cache.valueForVertex(vertexPosition);
	}
	else
	{
		maxSum = triangle->valueForIndexAtLine(vertexPosition.lineNumber, vertexPosition.index);
		
		bool existLeft = existLeftForVertex(triangle, vertexPosition);
		bool existRight = existRightForVertex(triangle, vertexPosition);
		int64_t leftMaxSum = std::numeric_limits<int64_t>::min();
		int64_t rightMaxSum = std::numeric_limits<int64_t>::min();
		if (existLeft)
		{
			VertexPosition left = leftVertexPositionForVertex(vertexPosition);
			leftMaxSum = maxSumFromVertexForTriangle(triangle, left);
		}
		if (existRight)
		{
			VertexPosition right = rightVertexPositionForVertex(vertexPosition);
			rightMaxSum = maxSumFromVertexForTriangle(triangle, right);
		}
		if (existRight && existLeft)
		{
			maxSum += MAX(leftMaxSum, rightMaxSum);
		}
	}
	
	return maxSum;
}


void prepareCacheForTriangle(const Triangle* triangle)
{
	size_t step = 5;
	if (triangle->countLines() > step)
	{
		int64_t lineNumber = triangle->countLines() - step;
		while (lineNumber >= 0)
		{
			for (size_t index = 0; index < triangle->countItemsOnLine(size_t(lineNumber)); index++)
			{
				VertexPosition position = {size_t(lineNumber), index};
				uint64 maxsum = maxSumFromVertexForTriangle(triangle, position);
				cache.setValueForVertex(position, maxsum);
			}
			lineNumber -= step;
		}
	}
}


int64_t maxSumForTriangleWithCache(const Triangle* triangle)
{
	VertexPosition startPosition = {0, 0};
	prepareCacheForTriangle(triangle);
	int64_t maxSum = maxSumFromVertexForTriangle(triangle, startPosition);
	
	return maxSum;
}


int main(int argc, const char * argv[]) {
	@autoreleasepool {
	    // insert code here...
		Triangle triangle = loadTriangleFromFile("BigTriangle.txt");
		//Triangle triangle = loadTriangleFromFile("SmallTriangle.txt");
		std::cout<<triangle<<"\n";
		
		//int64_t oldMaxSum = maxSumForTriangle(&triangle);
		//std::cout << "old maxSum "<< oldMaxSum<<"\n";
		int64_t maxSum = maxSumForTriangleWithCache(&triangle);
		std::cout<<"maxSum = " << maxSum<<"\n";
		
	}
	return 0;
}
