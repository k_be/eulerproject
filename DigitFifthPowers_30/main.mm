//
//  main.m
//  DigitFifthPowers_30
//
//  Created by Andrew Romanov on 21/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <vector>
#import <numeric>
#import "RavTimeLoger.h"


/*
 https://projecteuler.net/problem=30
 
 Surprisingly there are only three numbers that can be written as the sum of fourth powers of their digits:
 
 1634 = 1^4 + 6^4 + 3^4 + 4^4
 8208 = 8^4 + 2^4 + 0^4 + 8^4
 9474 = 9^4 + 4^4 + 7^4 + 4^4
 As 1 = 1^4 is not a sum it is not included.
 
 The sum of these numbers is 1634 + 8208 + 9474 = 19316.
 
 Find the sum of all the numbers that can be written as the sum of fifth powers of their digits.
 */

typedef uint64_t Number_t;
const Number_t topBorder = pow(Number_t(9), Number_t(5)) * 6;
typedef std::vector<Number_t> NumbersList_t;

NumbersList_t powersList;

void fillPowersList()
{
	for (Number_t num = 0; num <= 9; num++)
	{
		powersList.push_back(pow(num, 5));
	}
}


CG_INLINE Number_t powerForNumber(Number_t num)
{
	Number_t power = powersList.at(num);
	return power;
}


bool canRepresentNumberAsSumOfSteps(Number_t number)
{
	Number_t forMultiplication = number;
	Number_t sum = 0;
	bool can = YES;
	do {
		Number_t digit = forMultiplication % Number_t(10);
		Number_t power = powerForNumber(digit);
		sum = power + sum;
		can = (sum <= number);
		
		forMultiplication /= 10;
	}while(can && forMultiplication > 0);
		
	can = (sum == number);
	return can;
}

int main(int argc, const char * argv[]) {
	@autoreleasepool {
		RavTimeLoger* loger = [[RavTimeLoger alloc] initWithName:@"DigitFifthPowers"];
		
		fillPowersList();
		
		__block NumbersList_t list;
		
		for (Number_t num = 10; num < topBorder; num++)
		{
			if (canRepresentNumberAsSumOfSteps(num))
			{
				list.push_back(num);
			}
		}
		
		Number_t  sum = std::accumulate(list.cbegin(), list.cend(), Number_t(0));
		[loger printTime];
		NSLog(@"sum = %@", @(sum));
		
		powersList.clear();
		list.clear();
		RavTimeLoger* loger2 = [[RavTimeLoger alloc] initWithName:@"DigitFifthPowers_multithread"];
		fillPowersList();
		NSIndexSet* indexesSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(10, topBorder - 10)];
		
		__block dispatch_semaphore_t semathore = dispatch_semaphore_create(1);
		[indexesSet enumerateIndexesWithOptions:NSEnumerationConcurrent
																 usingBlock:^(NSUInteger idx, BOOL * _Nonnull stop) {
																	 if (canRepresentNumberAsSumOfSteps(idx))
																	 {
																		 dispatch_semaphore_wait(semathore, DISPATCH_TIME_FOREVER);
																		 list.push_back(idx);
																		 dispatch_semaphore_signal(semathore);
																	 }
																 }];
		Number_t  sum2 = std::accumulate(list.cbegin(), list.cend(), Number_t(0));
		[loger2 printTime];
		NSLog(@"sum = %@", @(sum2));
		
	}
	return 0;
}
