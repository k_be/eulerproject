//
//  main.m
//  NamesScores_22
//
//  Created by Andrew Romanov on 14/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>


uint64_t charactersSumForString(NSString* string)
{
	uint64_t sum = 0;
	uint64_t aCharacter = 'A';
	
	for (size_t charIndex = 0; charIndex < string.length; charIndex++)
	{
		uint64_t character = [string characterAtIndex:charIndex];
		if (character != UINT64_C(34))
		{
			uint64_t characterValue = character - aCharacter + 1;
			sum += characterValue;
		}
	}
	
	return sum;
}


int main(int argc, const char * argv[]) {
	@autoreleasepool {
	    // insert code here...
		NSError* loadingStringError = nil;
		NSString* fileContent = [NSString stringWithContentsOfFile:@"names.txt"
																											encoding:NSUTF8StringEncoding
																												 error:&loadingStringError];
		if (loadingStringError)
		{
			NSLog(@"can't load string");
		}
		
		NSMutableArray<NSString*>* namesWithQuotes = [[fileContent componentsSeparatedByString:@","] mutableCopy];
		
		[namesWithQuotes sortUsingSelector:@selector(compare:)];
		
		uint64_t sumForColin = charactersSumForString(@"\"COLIN\"");
		NSLog(@"sum for colin = %@", @(sumForColin));
		
		NSInteger indexOfColin = [namesWithQuotes indexOfObject:@"\"COLIN\""];
		NSLog(@"position in the list of COLIN %@", @(indexOfColin + 1));
		uint64_t colinScore = (indexOfColin + 1) * sumForColin;
		NSLog(@"score for colin %@", @(colinScore));
		
		__block uint64_t value = 0;
		
		[namesWithQuotes enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
			uint64_t charactersSum = charactersSumForString(obj);
			uint64_t score = charactersSum * (idx + 1);
			value += score;
		}];
		NSLog(@"sum score = %@", @(value));
		
		NSMutableArray<NSString* >* namesWithoutQuotes = [[NSMutableArray alloc] initWithCapacity:namesWithQuotes.count];
		NSCharacterSet* nonLettersCharacters = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
		[namesWithQuotes enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
			NSString* stringWithoutQuotes = [obj stringByTrimmingCharactersInSet:nonLettersCharacters];
			[namesWithoutQuotes addObject:stringWithoutQuotes];
		}];
		
		[namesWithoutQuotes sortUsingSelector:@selector(compare:)];
		value = 0;
		[namesWithoutQuotes enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
			uint64_t charactersSum = charactersSumForString(obj);
			uint64_t score = charactersSum * (idx + 1);
			
			value += score;
		}];
		NSLog(@"sum without quotes = %@", @(value));
		
	}
	return 0;
}
