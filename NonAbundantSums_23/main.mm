//
//  main.m
//  NonAbundantSums
//
//  Created by Andrew Romanov on 15/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 https://projecteuler.net/problem=23
 
 A perfect number is a number for which the sum of its proper divisors is exactly equal to the number. For example, the sum of the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which means that 28 is a perfect number.
 
 A number n is called deficient if the sum of its proper divisors is less than n and it is called abundant if this sum exceeds n.
 
 As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest number that can be written as the sum of two abundant numbers is 24. By mathematical analysis, it can be shown that all integers greater than 28123 can be written as the sum of two abundant numbers. However, this upper limit cannot be reduced any further by analysis even though it is known that the greatest number that cannot be expressed as the sum of two abundant numbers is less than this limit.
 
 Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.
*/

typedef uint64_t Number_t;

uint64_t sumOfDivisorsForNumber(Number_t number)
{
	Number_t sum = 1;
	
	for (uint64_t devider = 2; devider * devider <= number; devider ++)
	{
		if (number % devider == 0)
		{
			sum += devider;
			if (devider != number / devider)
			{
				sum += number / devider;
			}
		}
	}
	
	return sum;
}


BOOL isNumberAbundant(Number_t number)
{
	Number_t sumOfDeviders = sumOfDivisorsForNumber(number);
	BOOL abudant = (sumOfDeviders > number);
	return abudant;
}


NSIndexSet* abudantNumbersLessThenNumber(Number_t border)
{
	NSMutableIndexSet* numbers = [[NSMutableIndexSet alloc] init];
	[[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(1, border - 1)] enumerateIndexesWithOptions:kNilOptions
																																											 usingBlock:^(NSUInteger idx, BOOL * _Nonnull stop) {
																																												 if (isNumberAbundant(idx))
																																												 {
																																													 [numbers addIndex:idx];
																																												 }
																																											 }];
	return numbers;
}


NSIndexSet* numbersThatNotSumOfTwoNumbers(Number_t border, NSIndexSet* numbers)
{
	NSMutableIndexSet* easyNumbers = [[NSMutableIndexSet alloc] initWithIndexesInRange:NSMakeRange(UINT64_C(1), border)];
	[numbers enumerateIndexesWithOptions:kNilOptions
														usingBlock:^(NSUInteger firstComponent, BOOL * _Nonnull stop) {
															
															NSUInteger secondComponent = firstComponent;
															while (secondComponent != NSNotFound)
															{
																NSUInteger sum = secondComponent + firstComponent;
																if (sum <= border)
																{
																	[easyNumbers removeIndex:sum];
																}
																else
																{
																	break;
																}
																secondComponent = [numbers indexGreaterThanIndex:secondComponent];
															}
														}];
	return easyNumbers;
}


int main(int argc, const char * argv[]) {
	@autoreleasepool {
		
		NSIndexSet* abudantNumbers = abudantNumbersLessThenNumber(UINT64_C(28123));
		NSIndexSet* easyNumbers = numbersThatNotSumOfTwoNumbers(UINT64_C(28123), abudantNumbers);
		
		__block Number_t sum = 0;
		[easyNumbers enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL * _Nonnull stop) {
			NSLog(@"easy num = %@", @(idx));
			sum += idx;
		}];
		
		NSLog(@"sum = %@", @(sum));
	}
	return 0;
}
