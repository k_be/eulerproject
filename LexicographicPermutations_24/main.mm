//
//  main.m
//  LexicographicPermutations_24
//
//  Created by Andrew Romanov on 15/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <vector>
#import <iostream>

/*
 https://projecteuler.net/problem=24
 
 A permutation is an ordered arrangement of objects. For example, 3124 is one possible permutation of the digits 1, 2, 3 and 4. If all of the permutations are listed numerically or alphabetically, we call it lexicographic order. The lexicographic permutations of 0, 1 and 2 are:
 
 012   021   102   120   201   210
 
 What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
 */

//permutation on 725761th is 2/013456789

/*function nth_permutation($atoms, $index, $size) {
	for ($i = 0; $i < $size; $i++) {
		$item = $index % count($atoms);
		$index = floor($index / count($atoms));
		$result[] = $atoms[$item];
		array_splice($atoms, $item, 1);
	}
	return $result;
}*/

typedef char Element_t;
typedef std::vector<char> ElementsList_t;

uint64_t fact(uint64_t baseValue)
{
	uint64_t factVal = 1;
	if (baseValue > 1)
	{
		for (uint64_t mul = 2; mul <= baseValue; mul++)
		{
			factVal *= mul;
		}
	}
	
	return factVal;
}

ElementsList_t nth_premutation(ElementsList_t originalElements, uint64_t premutationIndex, uint64_t premutationLength)
{
	ElementsList_t premutation;
	
	for(uint64_t premutationElementIndex = 0; premutationElementIndex < premutationLength; premutationElementIndex++)
	{// p=i//math.factorial(cells-1-j)%len(source)
		uint64_t itemOriginalIndex = (premutationIndex / fact(uint64_t(premutationLength) - uint64_t(1) - uint64_t(premutationElementIndex))) % uint64_t(originalElements.size());
		premutation.push_back(originalElements.at(itemOriginalIndex));
		//premutationIndex = premutationIndex % uint64_t(originalElements.size());
		originalElements.erase(originalElements.begin() + itemOriginalIndex);
	}
	
	return premutation;
}

std::ostream& operator << (std::ostream& ostream, const ElementsList_t& elements)
{
	for (ElementsList_t::const_iterator elem = std::cbegin(elements);elem != std::cend(elements); elem++)
	{
		ostream << uint64_t(*elem);
		if (elem != std::cend(elements) - 1)
		{
			//ostream<<" ";
		}
	}
	
	return ostream;
}

int main(int argc, const char * argv[]) {
	@autoreleasepool {
		ElementsList_t originalElements({0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
		
		ElementsList_t premutation1 = nth_premutation(originalElements, UINT64_C(999999), originalElements.size());
		std::cout<<premutation1<<"\n";
		ElementsList_t premutation = nth_premutation(originalElements, UINT64_C(1000000), originalElements.size());
		std::cout<<premutation<<"\n";
		
		/*for (uint64_t i = 0; i < UINT64_C(1000000); i++)
		{
			ElementsList_t premutation1 = nth_premutation(originalElements, (i), originalElements.size());
			ElementsList_t premutation = nth_premutation(originalElements, (i+1), originalElements.size());
			
			if (premutation1 == premutation)
			{
				std::cout<<"equal"<<"\n";
				premutation1 = nth_premutation(originalElements, (i), originalElements.size());
				premutation = nth_premutation(originalElements, (i+1), originalElements.size());
			}
		}*/
	}
	return 0;
}
