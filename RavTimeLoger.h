//
//  RavTimeLoger.h
//  NumbersSum
//
//  Created by Andrew Romanov on 21/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RavTimeLoger : NSObject

@property (nonatomic, strong) NSString* name;

- (id)initWithName:(NSString*)name;

- (void)startRecording;
- (CFTimeInterval)reportTimeInterval;
- (CFTimeInterval)printTime;

@end
