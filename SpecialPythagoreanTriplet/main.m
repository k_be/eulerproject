//
//  main.m
//  SpecialPythagoreanTriplet
//
//  Created by Andrew Romanov on 07/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>


/*
 https://projecteuler.net/problem=9
 
 A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
 
 a^2 + b^2 = c^2
 For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
 
 There exists exactly one Pythagorean triplet for which a + b + c = 1000.
 Find the product abc.
 */

const NSUInteger sumCondition = 1000;

BOOL isPithagoreanTriplet(NSUInteger a, NSUInteger b, NSUInteger c)
{
	BOOL isTriplet = ((pow(a, 2) + pow(b, 2)) == pow(c, 2));
	return isTriplet;
}


int main(int argc, const char * argv[]) {
	@autoreleasepool {
		
		NSUInteger a = 1;
		NSUInteger b = 1;
		NSUInteger c = 1;
		
		for (a = 1; a < sumCondition - 1; a++)
		{
			for (b = a; b < sumCondition - a - 1; b++)
			{
				c = sumCondition - a - b;
				if (isPithagoreanTriplet(a, b, c))
				{
					break;
				}
			}
			
			if (isPithagoreanTriplet(a, b, c))
			{
				break;
			}
		}
		
		NSLog(@"a = %@, b = %@, c = %@", @(a), @(b), @(c));
		NSLog(@"truplet product = %@", @(a * b * c));
	}
	return 0;
}
