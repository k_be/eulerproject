//
//  Tree.hpp
//  BinaryTree_my
//
//  Created by Andrew Romanov on 20/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#ifndef Tree_hpp
#define Tree_hpp

#include <stdio.h>
#include <memory>


namespace rav
{
	template <typename DataT>
	class Tree {
	public:
		typedef std::shared_ptr<Tree> Tree_SP;
		
		Tree(DataT data, Tree_SP left, Tree_SP right)
		:_left(left), _right(right), _data(data)
		{}
		
		Tree(DataT data)
		:_left(nullptr), _right(nullptr), _data(data)
		{}
		
		inline void insertData(const DataT& data)
		{
			if (data < this->_data)
			{
				if (this->_left)
				{
					this->_left->insertData(data);
				}
				else
				{
					Tree_SP leftTree(new Tree(data));
					this->_left = leftTree;
				}
			}
			else
			{
				if (this->_right)
				{
					this->_right->insertData(data);
				}
				else
				{
					Tree_SP rightTree(new Tree(data));
					this->_right = rightTree;
				}
			}
		}
		
		inline const Tree* findData(const DataT& data) const
		{
			const Tree* pointer(nullptr);
			
			if (data == this->_data)
			{
				pointer = this;
			}
			else
			{
				if (data < this->_data)
				{
					if (this->_left)
					{
						pointer = _left->findData(data);
					}
				}
				else
				{
					if (this->_right)
					{
						pointer = _right->findData(data);
					}
				}
			}
			
			return pointer;
		}
		
		
		inline const DataT& getData() const
		{
			return _data;
		}
		
	private:
		Tree_SP _left;
		Tree_SP _right;
		DataT _data;
	};
};

#endif /* Tree_hpp */
