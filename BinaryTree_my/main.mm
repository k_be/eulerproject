//
//  main.m
//  BinaryTree_my
//
//  Created by Andrew Romanov on 20/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Tree.hpp"


int main(int argc, const char * argv[]) {
	@autoreleasepool {
		rav::Tree<int> tree(34);
		
		for (int i = 0; i < 100; i++)
		{
			tree.insertData(i);
		}
		
		const rav::Tree<int>* node = tree.findData(12);
		if (node)
		{
			NSLog(@"data = %@", @(node->getData()));
		}
		
	}
	return 0;
}
