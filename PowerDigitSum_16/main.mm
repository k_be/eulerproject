//
//  main.m
//  PowerDigitSum_16
//
//  Created by Andrew Romanov on 13/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <string>
#include <vector>

/*
 https://projecteuler.net/problem=16
 
 2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
 
 What is the sum of the digits of the number 2^1000?
 */

class TableOfMultiplication
{
public:
	
	static inline std::string multiplyCharacterTo2(char character)
	{
		std::string result = "0";
		
		switch (character)
		{
			case '0':
				result = "0";
				break;
			case '1':
				result = "2";
				break;
			case '2':
				result = "4";
				break;
			case '3':
				result = "6";
				break;
			case '4':
				result = "8";
				break;
			case '5':
				result = "10";
				break;
			case '6':
				result = "12";
				break;
			case '7':
				result = "14";
				break;
			case '8':
				result = "16";
				break;
			case '9':
				result = "18";
				break;
		};
		
		return result;
	}
	
	static inline uint characterValue(char character)
	{
		uint result = 0;
		
		switch (character)
		{
			case '0':
				result = 0;
				break;
			case '1':
				result = 1;
				break;
			case '2':
				result = 2;
				break;
			case '3':
				result = 3;
				break;
			case '4':
				result = 4;
				break;
			case '5':
				result = 5;
				break;
			case '6':
				result = 6;
				break;
			case '7':
				result = 7;
				break;
			case '8':
				result = 8;
				break;
			case '9':
				result = 9;
				break;
		};
		
		return result;
	}
	
};


class BigNumber
{
public:
	BigNumber(const std::string& stringRepresentation)
	{
		_reversedStorage = std::string(std::crbegin(stringRepresentation), std::crend(stringRepresentation));
	}
	
	std::string value() const
	{
		return std::string(std::crbegin(_reversedStorage), std::crend(_reversedStorage));
	}
	
	void addNumber(const BigNumber& otherNumber)
	{
		for (uint categoryIndex = 0; categoryIndex < otherNumber._reversedStorage.length(); categoryIndex++)
		{
			char character = otherNumber._reversedStorage[categoryIndex];
			addCharacterAtCategory(character, categoryIndex);
		}
	}
private:
	
	void addCharacterAtCategory(char character, uint category)
	{
		if (category >= _reversedStorage.length())
		{
			_reversedStorage.push_back(character);
		}
		else
		{
			uint intValue = characterIntValue(character);
			for (uint i = 0; i < intValue; i++)
			{
				bool needIncrementNext = incCategoryAtIndex(category);
				uint nextCategoryIndex = category + 1;
				while (needIncrementNext)
				{
					if (nextCategoryIndex >= _reversedStorage.length())
					{
						_reversedStorage.push_back('1');
						needIncrementNext = false;
					}
					else{
						needIncrementNext = incCategoryAtIndex(nextCategoryIndex);
						nextCategoryIndex++;
					}
				}
			}
		}
	}
	
	uint sumCharacters(char character1, char character2)
	{
		uint firstCharValue = characterIntValue(character1);
		uint secondCharValue = characterIntValue(character2);
		
		return firstCharValue + secondCharValue;
	}
	
	uint characterIntValue(char character)
	{
		std::vector<char>::const_iterator characterIndex = std::find(std::cbegin(_digitSequence), std::cend(_digitSequence), character);
		uint value = (uint)std::distance(std::cbegin(_digitSequence), characterIndex);
		
		return value;
	}
	
	//return YES if needs inc next category
	inline BOOL incCategoryAtIndex(uint categoryIndex)
	{
		char character = _reversedStorage[categoryIndex];
		char resultCharacter = incCharacter(character);
		_reversedStorage[categoryIndex] = resultCharacter;
		
		BOOL shouldIncNextCategory = character > resultCharacter;
		return shouldIncNextCategory;
	}
	
	inline char incCharacter(char character) const
	{
		std::vector<char>::const_iterator iterator = std::find(_digitSequence.cbegin(), _digitSequence.cend(), character);
		std::vector<char>::const_iterator nextCharacter = iterator + 1;
		if (nextCharacter == _digitSequence.cend())
		{
			nextCharacter = _digitSequence.begin();
		}
		
		return *nextCharacter;
	}
	
private:
	std::string _reversedStorage;
	static const std::vector<char> _digitSequence;
};

const std::vector<char> BigNumber::_digitSequence = std::vector<char>({'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'});


std::string multiplyStringNumberTo2(const std::string& number)
{
	BigNumber result("0");
	
	for (std::string::const_reverse_iterator iterator = std::crbegin(number); iterator != std::crend(number); iterator++)
	{
		long distance = std::distance(std::crbegin(number), iterator);
		char character = *iterator;
		std::string multiplicationResult = TableOfMultiplication::multiplyCharacterTo2(character);
		for(uint i = 0; i < distance; i++)
		{
			multiplicationResult.append("0");
		}
		
		result.addNumber(BigNumber(multiplicationResult));
	}
	
	return result.value();
}


UInt64 sumNumbers(const std::string& number)
{
	UInt64 result = 0;
	
	for(std::string::const_iterator iterator = number.cbegin(); iterator != number.cend(); iterator++)
	{
		UInt64 digitValue = TableOfMultiplication::characterValue(*iterator);
		result += digitValue;
	}
	
	return result;
}



int main(int argc, const char * argv[]) {
	@autoreleasepool {
		
		std::string result = "1";
		long powValue = 1000;
		for (int i  = 0; i < powValue; i++)
		{
			result = multiplyStringNumberTo2(result);
			//NSLog(@"string = %s", result.c_str());
		}
		
		UInt64 sum = sumNumbers(result);
		NSLog(@"sum = %@", @(sum));
	}
	return 0;
}
