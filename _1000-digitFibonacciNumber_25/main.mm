//
//  main.m
//  _1000-digitFibonacciNumber
//
//  Created by Andrew Romanov on 15/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <vector>
#include <sstream>
#include <string>
#include <iostream>


/*
 https://projecteuler.net/problem=25
 
 The Fibonacci sequence is defined by the recurrence relation:
 
 Fn = Fn−1 + Fn−2, where F1 = 1 and F2 = 1.
 Hence the first 12 terms will be:
 
 F1 = 1
 F2 = 1
 F3 = 2
 F4 = 3
 F5 = 5
 F6 = 8
 F7 = 13
 F8 = 21
 F9 = 34
 F10 = 55
 F11 = 89
 F12 = 144
 The 12th term, F12, is the first term to contain three digits.
 
 What is the index of the first term in the Fibonacci sequence to contain 1000 digits?
*/
class BigNumber
{
public:
	BigNumber(const std::string& stringRepresentation)
	{
		_reversedStorage = std::string(std::crbegin(stringRepresentation), std::crend(stringRepresentation));
	}
	
	std::string value() const
	{
		return std::string(std::crbegin(_reversedStorage), std::crend(_reversedStorage));
	}
	
	void addNumber(const BigNumber& otherNumber)
	{
		for (uint categoryIndex = 0; categoryIndex < otherNumber._reversedStorage.length(); categoryIndex++)
		{
			char character = otherNumber._reversedStorage[categoryIndex];
			addCharacterAtCategory(character, categoryIndex);
		}
	}
	
	void multiplyToNumber(UInt number)
	{
		if (number == 0)
		{
			_reversedStorage = "0";
		}
		else
		{
			BigNumber copied = *this;
			
			for (uint i = 1; i < number; i++)
			{
				this->addNumber(copied);
			}
		}
	}
	
	static uint characterIntValue(char character)
	{
		std::vector<char>::const_iterator characterIndex = std::find(std::cbegin(_digitSequence), std::cend(_digitSequence), character);
		uint value = (uint)std::distance(std::cbegin(_digitSequence), characterIndex);
		
		return value;
	}
	
private:
	
	void addCharacterAtCategory(char character, uint category)
	{
		if (category >= _reversedStorage.length())
		{
			_reversedStorage.push_back(character);
		}
		else
		{
			uint intValue = characterIntValue(character);
			for (uint i = 0; i < intValue; i++)
			{
				bool needIncrementNext = incCategoryAtIndex(category);
				uint nextCategoryIndex = category + 1;
				while (needIncrementNext)
				{
					if (nextCategoryIndex >= _reversedStorage.length())
					{
						_reversedStorage.push_back('1');
						needIncrementNext = false;
					}
					else{
						needIncrementNext = incCategoryAtIndex(nextCategoryIndex);
						nextCategoryIndex++;
					}
				}
			}
		}
	}
	
	uint sumCharacters(char character1, char character2)
	{
		uint firstCharValue = characterIntValue(character1);
		uint secondCharValue = characterIntValue(character2);
		
		return firstCharValue + secondCharValue;
	}
	
	//return YES if needs inc next category
	inline BOOL incCategoryAtIndex(uint categoryIndex)
	{
		char character = _reversedStorage[categoryIndex];
		char resultCharacter = incCharacter(character);
		_reversedStorage[categoryIndex] = resultCharacter;
		
		BOOL shouldIncNextCategory = character > resultCharacter;
		return shouldIncNextCategory;
	}
	
	inline char incCharacter(char character) const
	{
		std::vector<char>::const_iterator iterator = std::find(_digitSequence.cbegin(), _digitSequence.cend(), character);
		std::vector<char>::const_iterator nextCharacter = iterator + 1;
		if (nextCharacter == _digitSequence.cend())
		{
			nextCharacter = _digitSequence.begin();
		}
		
		return *nextCharacter;
	}
	
private:
	std::string _reversedStorage;
	static const std::vector<char> _digitSequence;
};

const std::vector<char> BigNumber::_digitSequence = std::vector<char>({'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'});


int main(int argc, const char * argv[]) {
	@autoreleasepool {
		BigNumber prevFibonachiNumber("1");
		BigNumber fibonachiNumber("2");
		uint64_t numberIndex = UINT64_C(3);
		
		while (fibonachiNumber.value().length() < 1000)
		{
			BigNumber currentValue = fibonachiNumber;
			fibonachiNumber.addNumber(prevFibonachiNumber);
			prevFibonachiNumber = currentValue;
			numberIndex++;
		}
		
		std::cout<<"index = "<< numberIndex<<"\n";
	}
	return 0;
}
