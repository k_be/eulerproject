//
//  RavTimeLoger.m
//  NumbersSum
//
//  Created by Andrew Romanov on 21/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import "RavTimeLoger.h"


@interface RavTimeLoger ()

@property (nonatomic) CFTimeInterval startTime;

@end


@implementation RavTimeLoger

- (id)initWithName:(NSString*)name
{
	if (self  = [super init])
	{
		self.name = name;
		
		[self startRecording];
	}
	
	return self;
}

- (void)setName:(NSString *)name
{
	if (!name)
	{
		name = @"";
	}
	
	_name = name;
}


- (void)startRecording
{
	_startTime = CFAbsoluteTimeGetCurrent();
}


- (CFTimeInterval)reportTimeInterval
{
	CFTimeInterval currentTime = CFAbsoluteTimeGetCurrent();
	CFTimeInterval interval = currentTime - _startTime;
	return interval;
}


- (CFTimeInterval)printTime
{
	CFTimeInterval interval = [self reportTimeInterval];
	NSLog(@"interval for task %@ = %@ s", _name, @(interval));
	return interval;
}

@end
