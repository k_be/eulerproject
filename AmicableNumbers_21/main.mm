//
//  main.m
//  AmicableNumbers_21
//
//  Created by Andrew Romanov on 14/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <set>
#include <numeric>
#include <iostream>


/*
https://projecteuler.net/problem=21
 
 Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
 If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and each of a and b are called amicable numbers.
 
 For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.
 
 Evaluate the sum of all the amicable numbers under 10000.
*/

typedef UInt64 PrimeNumber_t;
typedef UInt64 Number_t;
typedef std::set<PrimeNumber_t> NumbersContainer_t;

//animcable numbers
NumbersContainer_t amicableNumbers;
Number_t findSumOfDevidersForNumber(Number_t number)
{
	Number_t sum = 0;
	for (Number_t devider = 1; devider <= Number_t(ceil(sqrt(number))); devider++)
	{
		if (number % devider == 0)
		{
			sum += devider;
			if (devider != 1)
			{
				sum += (number / devider);
			}
		}
	}
	
	return sum;
}


Number_t sumOfNumbers(const NumbersContainer_t& numbers)
{
	Number_t sum = std::accumulate(std::cbegin(numbers), std::cend(numbers), Number_t(0));
	return sum;
}



void findAmicableNumbersBeforeBorder(Number_t border)
{
	for (Number_t firstNum = 1; firstNum < border - 1; firstNum++)
	{
		if (amicableNumbers.find(firstNum) == std::cend(amicableNumbers))
		{
			Number_t sum = findSumOfDevidersForNumber(firstNum);
			for (Number_t secondNum = firstNum + 1; secondNum < border; secondNum++)
			{
				Number_t secondSum = findSumOfDevidersForNumber(secondNum);
				if (secondSum == firstNum && sum == secondNum)
				{
					amicableNumbers.insert(firstNum);
					amicableNumbers.insert(secondNum);
				}
			}
		}
	}
}


int main(int argc, const char * argv[]) {
	@autoreleasepool {
		
		findAmicableNumbersBeforeBorder(10000);
		Number_t sum = sumOfNumbers(amicableNumbers);
		
		std::cout << "Sum = "<<sum<<"\n";
	}
	return 0;
}
