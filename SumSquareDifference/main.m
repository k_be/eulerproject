//
//  main.m
//  SumSquareDifference
//
//  Created by Andrew Romanov on 06/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 The sum of the squares of the first ten natural numbers is,
 
 1^2 + 2^2 + ... + 10^2 = 385
 The square of the sum of the first ten natural numbers is,
 
 (1 + 2 + ... + 10)^2 = 55^2 = 3025
 Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.
 
 Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
 */

NSInteger sumToNum(NSInteger num)
{
	NSInteger result = 0;
	if (num > 0)
	{
		result = ((double)(1 + num) / 2.0) * (double)num;
	}
	
	return result;
}


NSInteger sumOfSquaresFormula(NSInteger maxNum)
{
	CGFloat maxNumF = maxNum;
	
	NSInteger sum = (maxNumF / 6.0) * (2 * maxNumF + 1) * (maxNumF + 1);
	return sum;
}


NSInteger sumOfSquares(NSInteger maxNum)
{
	NSInteger sum = 0;
	for (NSInteger i = 1; i <= maxNum; i++)
	{
		sum += pow(i, 2);
	}
	
	return sum;
}


NSInteger sqrSum(NSInteger maxNum)
{
	NSInteger result = pow(sumToNum(maxNum), 2);
	return result;
}


int main(int argc, const char * argv[]) {
	@autoreleasepool {
	    // insert code here...
		NSInteger maxNum = 100;
		NSInteger result = sqrSum(maxNum) - sumOfSquares(maxNum);
		NSLog(@"diff = %@", @(result));
	}
	return 0;
}
