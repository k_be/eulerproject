//
//  main.m
//  LongestCollatzSequence
//
//  Created by Andrew Romanov on 09/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>
/*
 https://projecteuler.net/problem=14
 
 The following iterative sequence is defined for the set of positive integers:
 
 n → n/2 (n is even)
 n → 3n + 1 (n is odd)
 
 Using the rule above and starting with 13, we generate the following sequence:
 
 13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
 It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.
 
 Which starting number, under one million, produces the longest chain?
 
 NOTE: Once the chain starts the terms are allowed to go above one million.
 
 */

UInt64 nextValue(UInt64 prevValue)
{
	UInt64 result = prevValue;
	if (prevValue % 2ul == 0)
	{
		result = prevValue / 2ul;
	}
	else
	{
		result = 3 * prevValue + 1;
	}
	
	return result;
}



int main(int argc, const char * argv[]) {
	@autoreleasepool {
	    // insert code here...
		NSIndexSet* indexes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(1, 1000000)];
		
		NSLock* lock = [[NSLock alloc] init];
		__block UInt64 maxLength = 0;
		__block UInt64 num = 0;
		[indexes enumerateIndexesWithOptions:NSEnumerationConcurrent
															usingBlock:^(NSUInteger idx, BOOL * _Nonnull stop) {
																UInt64 length = 1;
																UInt64 seqValue = idx;
																while (seqValue != 1)
																{
																	length ++;
																	seqValue = nextValue(seqValue);
																}
																
																[lock lock];
																if (maxLength < length)
																{
																	maxLength = length;
																	num = idx;
																}
																[lock unlock];
															}];
		NSLog(@"max sequence %@", @(maxLength));
		NSLog(@"starts from number %@", @(num));
	}
	return 0;
}
