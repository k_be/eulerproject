//
//  main.m
//  CountingSundays_19
//
//  Created by Andrew Romanov on 14/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 https://projecteuler.net/problem=19
 
 You are given the following information, but you may prefer to do some research for yourself.
 
 1 Jan 1900 was a Monday.
 Thirty days has September,
 April, June and November.
 All the rest have thirty-one,
 Saving February alone,
 Which has twenty-eight, rain or shine.
 And on leap years, twenty-nine.
 A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
 How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?
 */

void incMonth(NSDateComponents* components)
{
	if (components.month == 12)
	{
		components.month = 1;
		components.year += 1;
	}
	else
	{
		components.month += 1;
	}
}


int main(int argc, const char * argv[]) {
	@autoreleasepool {
		NSCalendar* calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
		
		NSDateComponents* dateComponents = [[NSDateComponents alloc] init];
		dateComponents.calendar = calendar;
		dateComponents.year = 1901;
		dateComponents.month = 1;
		dateComponents.day = 1;
		
		NSInteger count = 0;
		do{
			NSDate* date = [calendar dateFromComponents:dateComponents];
			NSInteger weekDay = [calendar component:NSCalendarUnitWeekday fromDate:date];
			if (weekDay == 1)
			{
				count ++;
			}
			incMonth(dateComponents);
		}while (dateComponents.year < 2001);
		
		NSLog(@"count %@", @(count));
	}
	return 0;
}
