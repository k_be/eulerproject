//
//  main.m
//  Prime-10001st
//
//  Created by Andrew Romanov on 06/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <vector>

/*
 https://projecteuler.net/problem=7
 By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
 
 What is the 10 001st prime number?
 */

typedef unsigned long long PrimeNumber_t;
std::vector<PrimeNumber_t> primeNumbers({2, 3, 5, 7});

bool isAPrimeNumber(PrimeNumber_t number)
{
	BOOL isPrime = YES;
	for (int i = 0; i < primeNumbers.size(); i++)
	{
		PrimeNumber_t devider = primeNumbers.at(i);
		isPrime = ((number % devider) != 0);
		if (!isPrime)
		{
			break;
		}
	}
	
	return isPrime;
}



int main(int argc, const char * argv[]) {
	@autoreleasepool {
		
		PrimeNumber_t num = 11;
		while (primeNumbers.size() != 10001)
		{
			if (isAPrimeNumber(num))
			{
				primeNumbers.push_back(num);
			}
			
			num += 2;
		}
		
		PrimeNumber_t last = primeNumbers.back();
		NSLog(@"10001'st number = %@", @(last));
	}
	return 0;
}
