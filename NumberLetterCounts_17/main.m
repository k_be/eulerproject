//
//  main.m
//  NumberLetterCounts_17

//
//  Created by Andrew Romanov on 13/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 https://projecteuler.net/problem=17
 
 If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
 
 If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?
 
 
 NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20 letters. The use of "and" when writing out numbers is in compliance with British usage.
*/

@interface NumberFormatterWithAnd : NSNumberFormatter
{
}

@end


@implementation NumberFormatterWithAnd

- (instancetype)init
{
	if (self = [super init])
	{
		self.numberStyle = NSNumberFormatterSpellOutStyle;
		self.locale = [NSLocale localeWithLocaleIdentifier:@"En_us"];
	}
	
	return self;
}

- (NSString *)stringFromNumber:(NSNumber *)number
{
	long long longValue = [number longLongValue];
	
	NSString* resultString = @"";
	if (longValue > 100 &&
			longValue < 1000 &&
			longValue % 100 != 0)
	{
		long long bigPart = (longValue - longValue % (long long)(100));
		long long smallPart = (longValue - bigPart);
		
		NSMutableString* string = [[NSMutableString alloc] init];
		[string appendString:[super stringFromNumber:@(bigPart)]];
		[string appendString:@" and "];
		[string appendString:[super stringFromNumber:@(smallPart)]];
		
		resultString = string;
	}
	else
	{
		resultString = [super stringFromNumber:number];
	}
	
	return resultString;
}

@end


NSInteger countOfLettersInString(NSString* string)
{
	static NSCharacterSet* lettersCharacters = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		lettersCharacters = [NSCharacterSet letterCharacterSet];
	});
	
	NSInteger count = 0;
	for (int i = 0; i < string.length; i++)
	{
		char character = [string characterAtIndex:i];
		if ([lettersCharacters characterIsMember:character])
		{
			count++;
		}
	}
	
	return count;
}


int main(int argc, const char * argv[]) {
	@autoreleasepool {
		NSNumberFormatter* numberFormatter = [[NumberFormatterWithAnd alloc] init];
		
		NSInteger sum = 0;
		for (int i  = 1; i <= 1000; i++)
		{
			NSInteger countCharacters = countOfLettersInString([numberFormatter stringFromNumber:@(i)]);
			sum += countCharacters;
		}
		
		NSString* string = [numberFormatter stringFromNumber:@(342)];
		NSLog(@"strign = %@, length = %@", string, @(countOfLettersInString(string)));
		
		NSLog(@"sum = %@", @(sum));
	}
	return 0;
}
