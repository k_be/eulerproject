//
//  main.m
//  SmallestMultiple
//
//  Created by Andrew Romanov on 06/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
 What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
 */


BOOL isDevideOnAllNumbers(NSInteger number, NSInteger devidersMax)
{
	BOOL devide = YES;
	
	for (NSInteger devider = 2; devider <= devidersMax ; devider++)
	{
		devide = (number % devider == 0);
		if (!devide)
		{
			break;
		}
	}
	
	return devide;
}


int main(int argc, const char * argv[]) {
	@autoreleasepool {
		
		NSInteger deviable = 2 * 3 * 5 * 7 * 11 * 13 * 17 * 19;
		//NSInteger deviable = 2 * 3 * 5 * 7 * 11 * 13 * 17 * 19;
		
		while (!isDevideOnAllNumbers(deviable, 20))
		{
			deviable += 2;
		}
		
		NSLog(@"min deviable = %@", @(deviable));
		
	}
	return 0;
}
