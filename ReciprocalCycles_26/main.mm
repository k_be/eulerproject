//
//  main.m
//  ReciprocalCycles_26
//
//  Created by Andrew Romanov on 16/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <string>
#import <vector>


/*
 https://projecteuler.net/problem=26
 
 A unit fraction contains 1 in the numerator. The decimal representation of the unit fractions with denominators 2 to 10 are given:
 
 1/2	= 	0.5
 1/3	= 	0.(3)
 1/4	= 	0.25
 1/5	= 	0.2
 1/6	= 	0.1(6)
 1/7	= 	0.(142857)
 1/8	= 	0.125
 1/9	= 	0.(1)
 1/10	= 	0.1
 Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle. It can be seen that 1/7 has a 6-digit recurring cycle.
 
 Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.
*/


class UnitFraction
{
public:
	UnitFraction(uint d)
	:_d(d)
	{}
	
	size_t lengthOfRecurringCycle()
	{
		mods.clear();
		
		uint number = 1.0;
		BOOL findNext = YES;
		uint maxLength = 10000;
		uint step = 0;
		while (number != 0 && findNext && maxLength > step)
		{
			number = number % _d;
			number *= 10;
			if (modWasInModsList(number))
			{
				findNext = NO;
			}
			else
			{
				if (number != 0)
				{
					mods.push_back(number);
				}
			}
			step ++;
		}
		size_t length = 0;
		if (modWasInModsList(number))
		{
			Reminders_t::const_reverse_iterator it = std::find(mods.crbegin(), mods.crend(), number);
			length = std::distance(mods.crbegin(), it) + 1;
		}
		
		return length;
	}
private:
	typedef std::vector<uint> Reminders_t;
	
	bool modWasInModsList(uint mod) const
	{
		Reminders_t::const_iterator modIt = std::find(mods.cbegin(), mods.cend(), mod);
		return modIt != mods.cend();
	}
	
private:
	uint _d;
	
	Reminders_t mods;
	
};



int main(int argc, const char * argv[]) {
	@autoreleasepool {
		
		size_t maxCicleNumber = 1;
		size_t maxCicleLength = 0;
		for (uint num = 1; num < 1000; num++)
		{
			UnitFraction number(num);
			size_t cicleLengthForNum = number.lengthOfRecurringCycle();
			if (cicleLengthForNum > maxCicleLength)
			{
				maxCicleLength = cicleLengthForNum;
				maxCicleNumber = num;
			}
		}
		
		NSLog(@"max cycle = %@, for number = %@", @(maxCicleLength), @(maxCicleNumber));
		
	}
	return 0;
}
