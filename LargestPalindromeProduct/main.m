//
//  main.m
//  LargestPalindromeProduct
//
//  Created by Andrew Romanov on 06/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>


/*
 A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
 Find the largest palindrome made from the product of two 3-digit numbers.
 */

NSInteger countDigitsInNumber(NSInteger number)
{
	NSInteger count = 0;
	
	if (number > 0)
	{
		do
		{
			count ++;
			number = number / 10;
		}while(number > 0);
	}
	
	return count;
}

//indexes from left to right
//from 0
NSInteger getDigitAtIndex(NSInteger number, NSInteger index)
{
	NSInteger countDigits = countDigitsInNumber(number);
	NSInteger indexFromRight = countDigits - index - 1;
	NSInteger numberWithLastDigit = (indexFromRight > 0) ? number / (pow(10,indexFromRight)) : number;
	
	NSInteger digit = numberWithLastDigit % 10;
	
	return digit;
}


BOOL isPolindrome(NSInteger number)
{
	NSInteger countDigits = countDigitsInNumber(number);
	NSInteger countSteps = countDigits / 2;
	
	BOOL isPolindrome = YES;
	for (NSInteger i = 0; i < countSteps; i++)
	{
		NSInteger fromLeftDigit = getDigitAtIndex(number, i);
		NSInteger fromRightDigit = getDigitAtIndex(number, countDigits - i - 1);
		isPolindrome = fromLeftDigit == fromRightDigit;
		if (!isPolindrome)
		{
			break;
		}
  }
	
	return isPolindrome;
}


int main(int argc, const char * argv[]) {
	@autoreleasepool {
		NSLog(@"is polindrome 444 = %@", @(isPolindrome(444)));
		NSLog(@"isPolindrome 2345 = %@", @(isPolindrome(2345)));
		NSLog(@"isPolindrome 22322 = %@", @(isPolindrome(22322)));
		
		long long maxPolindrome = 0;
		long long mul1 = 0;
		long long mul2 = 0;
		
		for(long long multiplyer1 = 100; multiplyer1 < 1000; multiplyer1 ++)
		{
			for (long long multiplyer2 = multiplyer1; multiplyer2 < 1000; multiplyer2++)
			{
				long long result = multiplyer1 * multiplyer2;
				if (isPolindrome(result) && result > maxPolindrome)
				{
					maxPolindrome = result;
					mul1 = multiplyer1;
					mul2 = multiplyer2;
				}
			}
		}
		
		NSLog(@"max polindrome = %@", @(maxPolindrome));
		NSLog(@"mul1 = %@", @(mul1));
		NSLog(@"mul2 = %@", @(mul2));
		
	}
	return 0;
}
