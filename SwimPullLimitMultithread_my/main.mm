//
//  main.m
//  SwimpullLimit_My
//
//  Created by Andrew Romanov on 20/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <vector>
#import <algorithm>
#import <string>
#import <fstream>
#import <set>

/*
 Дан массив чисел - это высоты столбов (на рисунке, 2 примера), вывести номера столбов, налив воду между которыми, будет получен максимальный объём. Толщина столба 0, если уровень воды выше, то она переливается, если в ровень, то нет.
 */


typedef uint64_t Level_t;

std::vector<Level_t> loadLevelsFromFile(const std::string& fileName)
{
	std::vector<Level_t> levels;
	
	std::ifstream readingStream(fileName);
	if (!readingStream.is_open())
	{
		NSLog(@"can't open the file");
	}
	Level_t level(0);
	while (!(readingStream>>level).eof())
	{
		levels.push_back(level);
	}
	readingStream.close();
	
	return levels;
}


Level_t nextMinAfterLevel(const std::vector<Level_t>& levels, Level_t bottomBorder)
{
	Level_t min = *(std::max_element(levels.cbegin(), levels.cend()));
	
	for (std::vector<Level_t>::const_iterator it = levels.cbegin(); it != levels.end(); it++)
	{
		if (*it > bottomBorder && min > *it)
		{
			min = *it;
		}
	}
	
	return min;
}

typedef std::vector<Level_t> Levels_t;

@interface FindMaxLevelOperation : NSOperation
{}

@property (nonatomic) Level_t level;
@property (nonatomic) const Levels_t* levels;

@property (nonatomic, readonly) Levels_t::const_iterator maxFrom;
@property (nonatomic, readonly) Levels_t::const_iterator maxTo;
@property (nonatomic, readonly) Level_t maxCapacity;

@property (nonatomic, copy) void(^complition)(FindMaxLevelOperation* sender);

- (id)initWithLevel:(Level_t)level levelsList:(const Levels_t*)levelsList;


@end


@implementation FindMaxLevelOperation

- (id)initWithLevel:(Level_t)level levelsList:(const Levels_t *)levelsList
{
	if (self  = [super init])
	{
		_level = level;
		_levels = levelsList;
	}
	
	return self;
}


- (void)main
{
	_maxCapacity = 0;
	_maxFrom = _levels->cend();
	_maxTo = _levels->cend();
	
	Levels_t::const_iterator prevValue = _levels->cend();
	for (Levels_t::const_iterator valueIt = _levels->cbegin(); valueIt != _levels->cend(); valueIt++)
	{
		bool crossDevider = *valueIt >= _level;
		
		if (crossDevider)
		{
			if (prevValue != _levels->cend())
			{
				Level_t capacity = static_cast<Level_t>((std::distance(_levels->cbegin(), valueIt) - std::distance(_levels->cbegin(), prevValue))) * _level;
				if (capacity > _maxCapacity)
				{
					if (capacity == UINT64_C(149686769865178))
					{
						NSLog(@"break");
						NSLog(@"distance from = %@", @(std::distance(_levels->cbegin(), prevValue)));
						NSLog(@"distance to = %@", @(std::distance(_levels->cbegin(), valueIt)));
					}
					
					_maxCapacity = capacity;
					_maxFrom = prevValue;
					_maxTo = valueIt;
				}
				
			}
			prevValue = valueIt;
		}
	}
	
	if (self.complition)
	{
		self.complition(self);
	}
}

@end


@interface DataClass : NSObject

@property (nonatomic) Level_t capacity;
@property (nonatomic) Levels_t::const_iterator from;
@property (nonatomic) Levels_t::const_iterator to;

- (id)initWithCapacity:(Level_t)maxCapacity from:(Levels_t::const_iterator)from to:(Levels_t::const_iterator)to;

@end

@implementation DataClass

- (id)initWithCapacity:(Level_t)capacity from:(Levels_t::const_iterator)from to:(Levels_t::const_iterator)to
{
	if (self = [super init])
	{
		_capacity = capacity;
		_from = from;
		_to = to;
	}
	
	return self;
}

@end


int main(int argc, const char * argv[]) {
	@autoreleasepool {
		
		//Levels_t levels = loadLevelsFromFile("FloodPool10001");
		Levels_t levels = loadLevelsFromFile("FloodPool100001");
		//Levels_t levels({5, 4, 2, 5, 2, 3, 2, 3, 2, 4, 0, 7});
		
		std::set<Level_t> sortedLevels;
		for (Levels_t::const_iterator it = levels.cbegin(); it != levels.cend(); it++)
		{
			sortedLevels.insert((*it));
		}
		
		CFTimeInterval startTimeInterval = CFAbsoluteTimeGetCurrent();
		
		
		DataClass* maxValues = [[DataClass alloc] initWithCapacity:0
																										 from:levels.cend()
																											 to:levels.cend()];
		__block dispatch_semaphore_t semaphore = dispatch_semaphore_create(1);
		
		NSOperationQueue* queue = [[NSOperationQueue alloc] init];
		queue.qualityOfService = NSQualityOfServiceUserInteractive;
		queue.maxConcurrentOperationCount = 4;
		for (std::set<Level_t>::const_iterator sortedLevelsIterator = sortedLevels.cbegin(); sortedLevelsIterator != sortedLevels.cend(); sortedLevelsIterator ++)
		{
			while (queue.operationCount > 6)
			{
				[NSThread sleepForTimeInterval:0.01];
			}
			
			
				FindMaxLevelOperation* operation = [[FindMaxLevelOperation alloc] initWithLevel:*sortedLevelsIterator levelsList:&levels];
				operation.complition = ^(FindMaxLevelOperation *sender) {
					dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
					if (maxValues.capacity < sender.maxCapacity)
					{
						maxValues.capacity = sender.maxCapacity;
						maxValues.from = sender.maxFrom;
						maxValues.to = sender.maxTo;
					}
					dispatch_semaphore_signal(semaphore);
				};
				
				[queue addOperation:operation];
		}
		[queue waitUntilAllOperationsAreFinished];
		CFTimeInterval endTimeInterval = CFAbsoluteTimeGetCurrent();
		
		NSLog(@"from %@, to %@, capacity = %@", @(std::distance(levels.cbegin(), maxValues.from)), @(std::distance(levels.cbegin(), maxValues.to)), @(maxValues.capacity));
		NSLog(@"time capacity %@", @(endTimeInterval - startTimeInterval));
	}
	
	return 0;
}
