//
//  main.m
//  FactorialDigitSum
//
//  Created by Andrew Romanov on 14/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <vector>
#include <sstream>
#include <string>
#include <iostream>


/*
 https://projecteuler.net/problem=20
 
 n! means n × (n − 1) × ... × 3 × 2 × 1
 
 For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
 and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
 
 Find the sum of the digits in the number 100!
*/

class BigNumber
{
public:
	BigNumber(const std::string& stringRepresentation)
	{
		_reversedStorage = std::string(std::crbegin(stringRepresentation), std::crend(stringRepresentation));
	}
	
	std::string value() const
	{
		return std::string(std::crbegin(_reversedStorage), std::crend(_reversedStorage));
	}
	
	void addNumber(const BigNumber& otherNumber)
	{
		for (uint categoryIndex = 0; categoryIndex < otherNumber._reversedStorage.length(); categoryIndex++)
		{
			char character = otherNumber._reversedStorage[categoryIndex];
			addCharacterAtCategory(character, categoryIndex);
		}
	}
	
	void multiplyToNumber(UInt number)
	{
		if (number == 0)
		{
			_reversedStorage = "0";
		}
		else
		{
			BigNumber copied = *this;
			
			for (uint i = 1; i < number; i++)
			{
				this->addNumber(copied);
			}
		}
	}
	
	static uint characterIntValue(char character)
	{
		std::vector<char>::const_iterator characterIndex = std::find(std::cbegin(_digitSequence), std::cend(_digitSequence), character);
		uint value = (uint)std::distance(std::cbegin(_digitSequence), characterIndex);
		
		return value;
	}
	
private:
	
	void addCharacterAtCategory(char character, uint category)
	{
		if (category >= _reversedStorage.length())
		{
			_reversedStorage.push_back(character);
		}
		else
		{
			uint intValue = characterIntValue(character);
			for (uint i = 0; i < intValue; i++)
			{
				bool needIncrementNext = incCategoryAtIndex(category);
				uint nextCategoryIndex = category + 1;
				while (needIncrementNext)
				{
					if (nextCategoryIndex >= _reversedStorage.length())
					{
						_reversedStorage.push_back('1');
						needIncrementNext = false;
					}
					else{
						needIncrementNext = incCategoryAtIndex(nextCategoryIndex);
						nextCategoryIndex++;
					}
				}
			}
		}
	}
	
	uint sumCharacters(char character1, char character2)
	{
		uint firstCharValue = characterIntValue(character1);
		uint secondCharValue = characterIntValue(character2);
		
		return firstCharValue + secondCharValue;
	}
	
	//return YES if needs inc next category
	inline BOOL incCategoryAtIndex(uint categoryIndex)
	{
		char character = _reversedStorage[categoryIndex];
		char resultCharacter = incCharacter(character);
		_reversedStorage[categoryIndex] = resultCharacter;
		
		BOOL shouldIncNextCategory = character > resultCharacter;
		return shouldIncNextCategory;
	}
	
	inline char incCharacter(char character) const
	{
		std::vector<char>::const_iterator iterator = std::find(_digitSequence.cbegin(), _digitSequence.cend(), character);
		std::vector<char>::const_iterator nextCharacter = iterator + 1;
		if (nextCharacter == _digitSequence.cend())
		{
			nextCharacter = _digitSequence.begin();
		}
		
		return *nextCharacter;
	}
	
private:
	std::string _reversedStorage;
	static const std::vector<char> _digitSequence;
};

const std::vector<char> BigNumber::_digitSequence = std::vector<char>({'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'});


BigNumber bigFactorial(UInt toNumber)
{
	BigNumber number("1");
	
	for (UInt mul = 1; mul <= toNumber; mul++)
	{
		number.multiplyToNumber(mul);
	}
	
	return number;
}


UInt64 sumNumbers(const std::string& number)
{
	UInt64 result = 0;
	
	for(std::string::const_iterator iterator = number.cbegin(); iterator != number.cend(); iterator++)
	{
		UInt64 digitValue = BigNumber::characterIntValue(*iterator);
		result += digitValue;
	}
	
	return result;
}



int main(int argc, const char * argv[]) {
	@autoreleasepool {
		
		BigNumber fact = bigFactorial(100);
		std::cout<<"fact = "<<fact.value() << "\n";
		UInt64 sum = sumNumbers(fact.value());
		std::cout<<"Sum = "<<sum<<"\n";
	}
	return 0;
}
