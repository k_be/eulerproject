//
//  main.m
//  PrimeDigitReplacements_51
//
//  Created by Andrew Romanov on 15/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <set>
#import <vector>
#import <iostream>
#import <numeric>
#import <functional>



/*
 https://projecteuler.net/problem=51
 
 By replacing the 1st digit of the 2-digit number *3, it turns out that six of the nine possible values: 13, 23, 43, 53, 73, and 83, are all prime.
 
 By replacing the 3rd and 4th digits of 56**3 with the same digit, this 5-digit number is the first example having seven primes among the ten generated numbers, yielding the family: 56003, 56113, 56333, 56443, 56663, 56773, and 56993. Consequently 56003, being the first member of this family, is the smallest prime with this property.
 
 Find the smallest prime which, by replacing part of the number (not necessarily adjacent digits) with the same digit, is part of an eight prime value family.
*/
typedef unsigned long long Number_t;
typedef unsigned long long PrimeNumber_t;
typedef std::set<PrimeNumber_t> NumbersList_t;
NumbersList_t primeNumbers;

/*template <typename Iter>
Iter next(Iter iter)
{
	return ++iter;
}*/


bool isAPrimeNumber(PrimeNumber_t number)
{
	BOOL isPrime = YES;
	for (NumbersList_t::const_iterator i = std::cbegin(primeNumbers); i != std::cend(primeNumbers) && (*i) * (*i) <= number; i++)
	{
		PrimeNumber_t devider = *i;
		isPrime = ((number % devider) != 0);
		if (!isPrime)
		{
			break;
		}
	}
	
	return isPrime;
}


bool isNumberInPrimesList(Number_t number)
{
	bool inList = primeNumbers.find(number) != primeNumbers.end();
	return inList;
}


void fillPrimeNumbersBefor(Number_t border)
{
	for (Number_t number = 2; number < border; number++)
	{
		if (isAPrimeNumber(number))
		{
			primeNumbers.insert(number);
		}
	}
}


uint lengthOfNumber(Number_t number)
{
	uint length = 0;
	do{
		length++;
		number = number / 10;
	}while (number != 0);
		
	return length;
}


/*
 category from 0
 */
Number_t replaceDigitAtCategoryWithDigit(Number_t number, Number_t digit, uint category)
{
	Number_t resultNumber = number;
	if (digit <= 9)
	{
		Number_t categoryValue = number;
		for (uint i = 0; i < category; i++)
		{
			categoryValue /= 10;
		}
		categoryValue = categoryValue % 10;
		resultNumber = number - categoryValue * pow(10, category);
		resultNumber = resultNumber + digit * pow(10, category);
	}
	else
	{
		assert(1);
	}
	
	return resultNumber;
}


typedef std::vector<bool> Combination_t;
typedef std::vector<Combination_t> CombinationsList_t;
/*
 all combinations select k from n
 k > 0
 k <= n
 n > 0
*/

void combinationVariantsFromPosition(size_t n, size_t k, size_t fromIndex, size_t  itemInSelectionIndex, Combination_t& combination,void(^callback)(Combination_t combination))
{
	for (size_t position = fromIndex; position < n - (k - itemInSelectionIndex - 1); position++)
	{
		combination[position] = true;
		if (itemInSelectionIndex == k - 1)
		{
			callback(combination);
		}
		else
		{
			combinationVariantsFromPosition(n, k, position + 1, itemInSelectionIndex+1, combination, callback);
		}
		combination[position] = false;
	}
}


CombinationsList_t generateCombinations(size_t n, size_t k)
{
	__block CombinationsList_t list;
	
	Combination_t combination(n, false);
	combinationVariantsFromPosition(n, k, 0, 0, combination,^(Combination_t combination) {
		list.push_back(combination);
	});
	
	return list;
}


std::ostream& operator << (std::ostream& outstream, const Combination_t& combination)
{
	for (Combination_t::const_iterator iterator = std::cbegin(combination); iterator != std::cend(combination); iterator++)
	{
		outstream<<*iterator;
		if (iterator != std::cend(combination) - 1)
		{
			outstream << " ";
		}
	}
	
	return outstream;
}

std::ostream& operator << (std::ostream& outstream, const CombinationsList_t& list)
{
	for (CombinationsList_t::const_iterator iterator = std::cbegin(list); iterator != std::cend(list); iterator++)
	{
		outstream<<*iterator;
		if (iterator != std::cend(list) - 1)
		{
			outstream<<"\n";
		}
	}
	
	return outstream;
}


std::ostream& operator << (std::ostream& outstream, const NumbersList_t& numbers)
{
	for (NumbersList_t::const_iterator iterator = std::cbegin(numbers); iterator != std::cend(numbers); iterator++)
	{
		outstream<<*iterator;
		if (next(iterator) != numbers.cend())
		{
			outstream << " ";
		}
	}
	
	return outstream;
}


NumbersList_t generateNumbersFromNumber(Number_t number, const Combination_t& combination)
{
	NumbersList_t list;
	for (uint i = 0; i < 10; i++)
	{
		if (i != 0 || !(*combination.crbegin()) )
		{
			Number_t changedNumber = number;
			for (Combination_t::const_iterator replacement = combination.cbegin(); replacement != combination.cend(); replacement++)
			{
				if (*replacement)
				{
					changedNumber = replaceDigitAtCategoryWithDigit(changedNumber, i, uint(std::distance(combination.cbegin(), replacement)));
				}
			}
			
			list.insert(changedNumber);
		}
	}
	
	return list;
}


NumbersList_t removeNonPrimeNumbers(const NumbersList_t& list)
{
	NumbersList_t filteredList;
	
	for (NumbersList_t::const_iterator listIterator = list.begin(); listIterator != list.end(); listIterator++)
	{
		bool inList = isNumberInPrimesList(*listIterator);
		if(inList)
		{
			filteredList.insert(*listIterator);
		}
	}
	
	return filteredList;
}


PrimeNumber_t findPrimeMinNumberFromGroupWithLength(uint groupLength)
{
	PrimeNumber_t number = 0;
	
	for (NumbersList_t::const_iterator primeNumbersIterator = primeNumbers.begin(); primeNumbersIterator != primeNumbers.end(); primeNumbersIterator++)
	{
		uint length = lengthOfNumber(*primeNumbersIterator);
		for (uint countReplacements = 1; countReplacements < length; countReplacements++)
		{
			CombinationsList_t combinations = generateCombinations(length, countReplacements);
			
			for (CombinationsList_t::const_iterator combinationIt = combinations.begin(); combinationIt != combinations.end(); combinationIt++)
			{
				NumbersList_t numbers = generateNumbersFromNumber(*primeNumbersIterator, *combinationIt);
				numbers = removeNonPrimeNumbers(numbers);
				if (numbers.size() == groupLength)
				{
					number = *(std::min_element(numbers.cbegin(), numbers.cend()));
					break;
				}
			}
			if(number != 0)
			{
				break;
			}
		}
		if(number != 0)
		{
			break;
		}
	}
	
	return number;
}

int main(int argc, const char * argv[]) {
	@autoreleasepool {
		Number_t border = Number_t(10000000);
		fillPrimeNumbersBefor(border);
		NumbersList_t::const_reverse_iterator iterator = primeNumbers.crbegin();
		Number_t lastPrime = *(iterator);
		std::cout<<"lastPrime = "<<lastPrime<<"\n";
		
		
		PrimeNumber_t number = findPrimeMinNumberFromGroupWithLength(8);
		std::cout<< number << "\n";
		
		
		
		
		
	}
	return 0;
}
