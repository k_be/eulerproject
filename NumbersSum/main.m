//
//  main.m
//  NumbersSum
//
//  Created by Andrew Romanov on 06/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>

NSInteger sumToNum(NSInteger num)
{
	NSInteger result = 0;
	if (num > 0)
	{
		result = ((double)(1 + num) / 2.0) * (double)num;
	}
	
	return result;
}


NSInteger sumForMultiplyer(NSInteger multiplyer, NSInteger border)
{
	NSInteger sum = 0;
	if (border > 0 && multiplyer > 0)
	{
		NSInteger countItems = (border - 1) / multiplyer;
		sum = multiplyer * sumToNum(countItems);
	}
	
	return sum;
}


int main(int argc, const char * argv[]) {
	@autoreleasepool {
	    // insert code here...
		
		NSInteger border = 1000000000;
		
		NSInteger sumFor3 = sumForMultiplyer(3, border);
		NSInteger sumFor5 = sumForMultiplyer(5, border);
		NSInteger sumFor15 = sumForMultiplyer(3 * 5, border);
		
		NSInteger result = sumFor3 + sumFor5 - sumFor15;
		
		NSLog(@"resut = %@", @(result));
	}
	return 0;
}
