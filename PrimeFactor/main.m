//
//  main.m
//  PrimeFactor
//
//  Created by Andrew Romanov on 06/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 The prime factors of 13195 are 5, 7, 13 and 29.

 What is the largest prime factor of the number 600851475143 ?
 */

BOOL isItPrimeNumber(NSInteger number)
{
	NSInteger devider = 2;
	
	while (devider <= sqrt(number) && number % devider != 0)
	{
		devider ++;
	}
	BOOL prime = number % devider != 0;
	
	return prime;
}


int main(int argc, const char * argv[]) {
	@autoreleasepool {
		
		NSInteger number = 600851475143;
		NSInteger maxPrime = 0;
		
		for (NSInteger devider = 3; devider <= sqrt(number); devider+= 2)
		{
			if (number % devider == 0)
			{
				BOOL prime = isItPrimeNumber(devider);
				if (prime)
				{
					maxPrime = devider;
				}
			}
		}
		
		NSLog(@"max prime = %@", @(maxPrime));
	}
	return 0;
}
