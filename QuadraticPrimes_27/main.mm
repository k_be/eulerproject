//
//  main.m
//  QuadraticPrimes_27
//
//  Created by Andrew Romanov on 19/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <set>


/*
https://projecteuler.net/problem=27
 
 Euler discovered the remarkable quadratic formula:
 
 n^2 + n + 41
 It turns out that the formula will produce 40 primes for the consecutive integer values 0≤n≤39, 0≤n≤39. However, when n=40,402+40+41=40(40+1)+41n=40,402+40+41=40(40+1)+41 is divisible by 41, and certainly when n=41,412+41+41n=41,412+41+41 is clearly divisible by 41.
 
 The incredible formula n2−79n+1601n2−79n+1601 was discovered, which produces 80 primes for the consecutive values 0≤n≤790≤n≤79. The product of the coefficients, −79 and 1601, is −126479.
 
 Considering quadratics of the form:
 
 n2+an+bn2+an+b, where |a|<1000|a|<1000 and |b|≤1000|b|≤1000
 
 where |n||n| is the modulus/absolute value of nn
 e.g. |11|=11|11|=11 and |−4|=4|−4|=4
 Find the product of the coefficients, a and b, for the quadratic expression that produces the maximum number of primes for consecutive values of nn, starting with n=0n=0.
*/

typedef int64_t Number_t;
typedef unsigned long long PrimeNumber_t;
typedef std::set<PrimeNumber_t> NumbersList_t;
NumbersList_t primeNumbers;

/*template <typename Iter>
 Iter next(Iter iter)
 {
 return ++iter;
 }*/


bool isAPrimeNumber(PrimeNumber_t number)
{
	BOOL isPrime = YES;
	for (NumbersList_t::const_iterator i = std::cbegin(primeNumbers); i != std::cend(primeNumbers) && (*i) * (*i) <= number; i++)
	{
		PrimeNumber_t devider = *i;
		isPrime = ((number % devider) != 0);
		if (!isPrime)
		{
			break;
		}
	}
	
	return isPrime;
}


bool isNumberInPrimesList(Number_t number)
{
	bool inList = false;
	if (number > 0)
	{
		inList = primeNumbers.find(number) != primeNumbers.end();
	}
	return inList;
}


void fillPrimeNumbersBefor(Number_t border)
{
	for (Number_t number = 2; number < border; number++)
	{
		if (isAPrimeNumber(number))
		{
			primeNumbers.insert(number);
		}
	}
}


PrimeNumber_t maxPrimeNumber()
{
	PrimeNumber_t value = *(std::crbegin(primeNumbers));
	return value;
}


class QuadricFormula
{
public:
	QuadricFormula(Number_t a, Number_t b)
	:_a(a), _b(b)
	{}
	
	Number_t valueForNumber(Number_t number) const
	{
		Number_t value = number * number + _a * number + _b;
		return value;
	}
private:
	Number_t _a;
	Number_t _b;
};


size_t howManyPrimeNumbersForFormula(const QuadricFormula& formula)
{
	PrimeNumber_t number = maxPrimeNumber();
	size_t count = 0;
	
	PrimeNumber_t lastPrimeNumber = 0;
	
	for(Number_t n = 0; n < number; n++)
	{
		Number_t value = formula.valueForNumber(n);
		if (value > Number_t(number))
		{
			NSLog(@"warning: %@", @(value));
		}
		
		if (Number_t(lastPrimeNumber) != value &&
				value > 0 &&
				isNumberInPrimesList(value))
		{
			count++;
		}
		else
		{
			break;
		}
	}
	
	return count;
}


int main(int argc, const char * argv[]) {
	@autoreleasepool {
		NSLog(@"filing prime numbers ...");
		fillPrimeNumbersBefor(1000000);
		
		NSLog(@"finding coeff...");
		size_t maxCount = 0;
		Number_t maxA = 0;
		Number_t maxB = 0;
		for (Number_t a = -999; a <= 999; a++)
		{
			for (Number_t b = -1000; b <= 1000; b++)
			{
				QuadricFormula formula(a, b);
				size_t count  = howManyPrimeNumbersForFormula(formula);
				
				if (count > maxCount)
				{
					maxCount = count;
					maxA = a;
					maxB = b;
				}
			}
		}
		
		NSLog(@"maxCount = %@", @(maxCount));
		NSLog(@"a = %@", @(maxA));
		NSLog(@"b = %@", @(maxB));
		NSLog(@"product = %@", @(maxA * maxB));
	}
	return 0;
}
