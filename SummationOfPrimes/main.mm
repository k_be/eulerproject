//
//  main.m
//  SummationOfPrimes
//
//  Created by Andrew Romanov on 07/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <vector>
#import <numeric>


/*
 https://projecteuler.net/problem=10
 
 The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
 Find the sum of all the primes below two million.
 
 */
typedef UInt64 PrimeNumber_t;
std::vector<PrimeNumber_t> primeNumbers({2});

bool isAPrimeNumber(PrimeNumber_t number)
{
	BOOL isPrime = YES;
	
	CGFloat sqrtValue = sqrt(CGFloat(number));
	CGFloat deviderBorder =  ceil(sqrtValue);
	
	for (int i = 0; i < primeNumbers.size(); i++)
	{
		PrimeNumber_t devider = primeNumbers.at(i);
		
		if (devider <= deviderBorder)
		{
			isPrime = ((number % devider) != 0);
			if (!isPrime)
			{
				break;
			}
		}
		else
		{
			break;
		}
	}
	
	return isPrime;
}


void findPrimeNumbersBefore(PrimeNumber_t border)
{
	for (PrimeNumber_t number = 3; number < border; number += 2)
	{
		if (isAPrimeNumber(number))
		{
			primeNumbers.push_back(number);
		}
	}
}


int main(int argc, const char * argv[]) {
	@autoreleasepool {
		
		PrimeNumber_t numbersBorder = 2000000;
		
		findPrimeNumbersBefore(numbersBorder);
		
		PrimeNumber_t sum = std::accumulate(primeNumbers.cbegin(), primeNumbers.cend(), PrimeNumber_t(0));
		NSLog(@"sum = %@", @(sum));
	}
	return 0;
}
