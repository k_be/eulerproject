//
//  main.m
//  LatticePaths_15
//
//  Created by Andrew Romanov on 09/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <utility>
#import <vector>
#import <algorithm>
#import <map>

/*
 https://projecteuler.net/problem=15
 
 Starting in the top left corner of a 2×2 grid, and only being able to move to the right and down, there are exactly 6 routes to the bottom right corner.
 
 
 How many such routes are there through a 20×20 grid?
 
 */


class Field
{
public:
	Field(int width, int height)
	:_width(width), _height(height)
	{}
	
	typedef int PointIndex_t;
	typedef int Line_t;
	typedef int Collumn_t;
	typedef std::pair<Line_t, Collumn_t> Position_t;
	
	inline PointIndex_t pointIndexAtLineCollumn(Line_t line, Collumn_t collumn) const
	{
		PointIndex_t index = line * _width + collumn;
		return index;
	}
	
	inline Position_t pointPositionAtIndex(PointIndex_t index) const
	{
		Collumn_t collumn = index % _width;
		Line_t line = (index - collumn) / _width;
		
		return Position_t(line, collumn);
	}
	
	inline Collumn_t width() const
	{
		return _width;
	}
	
	inline Line_t height() const
	{
		return _height;
	}
private:
	int _width;
	int _height;
};


#define NO_CACHED_VALUE (-1)
typedef std::map<Field::PointIndex_t, int64_t> Cache_t;
std::map<Field::PointIndex_t, int64_t> cache;

int64_t cachedValueForPointIndex(Field::PointIndex_t index)
{
	Cache_t::const_iterator iterator = cache.find(index);
	int64_t value = iterator != cache.cend() ? iterator->second : NO_CACHED_VALUE;
	
	return value;
}


void setToCacheValueToIndex(Field::PointIndex_t index, int64_t value)
{
	cache.insert(Cache_t::value_type(index, value));
}


bool canStepToRightFromPoint(Field::PointIndex_t pointIndex, const Field& field)
{
	Field::Position_t position = field.pointPositionAtIndex(pointIndex);
	bool canStep = position.second < field.width() - 1;
	
	return canStep;
}


bool canstepToBottomFromPoint(Field::PointIndex_t pointIndex, const Field& field)
{
	Field::Position_t position = field.pointPositionAtIndex(pointIndex);
	bool canStep = position.first < field.height() - 1;
	return canStep;
}


Field::PointIndex_t goToDownFromPoint(Field::PointIndex_t fromPoint, const Field& field)
{
	Field::Position_t position = field.pointPositionAtIndex(fromPoint);
	Field::Position_t destinationPosition(position.first + 1, position.second);
	Field::PointIndex_t destinationPointIndex = field.pointIndexAtLineCollumn(destinationPosition.first, destinationPosition.second);
	return destinationPointIndex;
}

Field::PointIndex_t goToRightFromPoint(Field::PointIndex_t fromPoint, const Field& field)
{
	Field::Position_t position = field.pointPositionAtIndex(fromPoint);
	Field::Position_t destinationPosition(position.first, position.second + 1);
	Field::PointIndex_t destinationPointIndex = field.pointIndexAtLineCollumn(destinationPosition.first, destinationPosition.second);
	return destinationPointIndex;
}


int64_t countPathsFromPoint(Field::PointIndex_t point, const Field& field)
{
	int64_t count = int64_t(0);
	
	bool canStepDown = canstepToBottomFromPoint(point, field);
	if (canStepDown)
	{
		count ++;
	}
	bool canStepRight = canStepToRightFromPoint(point, field);
	if (canStepRight)
	{
		count ++;
	}
	
	if (canStepRight)
	{
		Field::PointIndex_t rightPoint = goToRightFromPoint(point, field);
		int64_t countPaths = cachedValueForPointIndex(rightPoint);
		if (countPaths == NO_CACHED_VALUE)
		{
			countPaths = (countPathsFromPoint(rightPoint, field));
			setToCacheValueToIndex(rightPoint, countPaths);
		}
		
		count +=  countPaths > int64_t(0) ?  countPaths - int64_t(1) : int64_t(0);
	}
	if (canStepDown)
	{
		Field::PointIndex_t downPoint = goToDownFromPoint(point, field);
		int64_t countPaths = cachedValueForPointIndex(downPoint);
		if (countPaths == NO_CACHED_VALUE)
		{
			countPaths = (countPathsFromPoint(downPoint, field));
			setToCacheValueToIndex(downPoint, countPaths);
		}
		
		count +=  countPaths > int64_t(0) ?  countPaths - int64_t(1) : int64_t(0);
	}
	
	return count;
}


int64_t calcPathsInField(const Field& field)
{
	Field::PointIndex_t startPoint = 0;
	int64_t count = countPathsFromPoint(startPoint, field);
	
	return count;
}


int main(int argc, const char * argv[]) {
	@autoreleasepool {
		Field field(21, 21);
		int64_t countPaths = calcPathsInField(field);
		NSLog(@"count paths %@", @(countPaths));
	}
	return 0;
}
