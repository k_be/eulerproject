//
//  main.m
//  SwimpullLimit_My
//
//  Created by Andrew Romanov on 20/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <vector>
#import <algorithm>
#import <string>
#import <fstream>
#import <set>

/*
 Дан массив чисел - это высоты столбов (на рисунке, 2 примера), вывести номера столбов, налив воду между которыми, будет получен максимальный объём. Толщина столба 0, если уровень воды выше, то она переливается, если в ровень, то нет.
*/


typedef uint64_t Level_t;

std::vector<Level_t> loadLevelsFromFile(const std::string& fileName)
{
	std::vector<Level_t> levels;
	
	std::ifstream readingStream(fileName);
	if (!readingStream.is_open())
	{
		NSLog(@"can't open the file");
	}
	while (!readingStream.eof())
	{
		Level_t level(0);
		readingStream >> level;
		
		levels.push_back(level);
	}
	readingStream.close();
	
	return levels;
}


Level_t nextMinAfterLevel(const std::vector<Level_t>& levels, Level_t bottomBorder)
{
	Level_t min = *(std::max_element(levels.cbegin(), levels.cend()));
	
	for (std::vector<Level_t>::const_iterator it = levels.cbegin(); it != levels.end(); it++)
	{
		if (*it > bottomBorder && min > *it)
		{
			min = *it;
		}
	}
	
	return min;
}


int main(int argc, const char * argv[]) {
	@autoreleasepool {
		typedef std::vector<Level_t> Levels_t;
		
		Levels_t levels = loadLevelsFromFile("FloodPool10001");
		//Levels_t levels = loadLevelsFromFile("FloodPool100001");
		//Levels_t levels({5, 4, 2, 5, 2, 3, 2, 3, 2, 4, 0, 7});
		
		std::set<Level_t> sortedLevels;
		for (Levels_t::const_iterator it = levels.cbegin(); it != levels.cend(); it++)
		{
			sortedLevels.insert((*it));
		}
		
			CFTimeInterval startTimeInterval = CFAbsoluteTimeGetCurrent();
		
		Level_t maxCapacity = 0;
		Levels_t::const_iterator fromMax = levels.cend();
		Levels_t::const_iterator toMax = levels.cend();
		std::set<Level_t>::const_iterator level = sortedLevels.begin();//*(std::min_element(levels.cbegin(), levels.cend()));
		while (level != sortedLevels.cend())
		{
			Levels_t::const_iterator prevValue = levels.cend();
			for (Levels_t::const_iterator valueIt = levels.cbegin(); valueIt != levels.cend(); valueIt++)
			{
				bool crossDevider = *valueIt >= *level;
				
				if (crossDevider)
				{
					if (prevValue != levels.cend())
					{
						Level_t capacity = static_cast<Level_t>((std::distance(levels.cbegin(), valueIt) - std::distance(levels.cbegin(), prevValue))) * *level;
						if (capacity > maxCapacity)
						{
							maxCapacity = capacity;
							fromMax = prevValue;
							toMax = valueIt;
						}
						
					}
					prevValue = valueIt;
				}
			}
			
			level++;
		}
		
		CFTimeInterval endTimeInterval = CFAbsoluteTimeGetCurrent();
		
		NSLog(@"from %@, to %@, capacity = %@", @(std::distance(levels.cbegin(), fromMax)), @(std::distance(levels.cbegin(), toMax)), @(maxCapacity));
		NSLog(@"time capacity %@", @(endTimeInterval - startTimeInterval));
		
		}
	
	return 0;
}
