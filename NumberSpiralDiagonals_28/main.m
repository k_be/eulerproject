//
//  main.m
//  NumberSpiralDiagonals_28
//
//  Created by Andrew Romanov on 19/06/2017.
//  Copyright © 2017 RAV. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
https://projecteuler.net/problem=28
 
 
 Starting with the number 1 and moving to the right in a clockwise direction a 5 by 5 spiral is formed as follows:
 
 21 22 23 24 25
 20  7  8  9 10
 19  6  1  2 11
 18  5  4  3 12
 17 16 15 14 13
 
 It can be verified that the sum of the numbers on the diagonals is 101.
 
 What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed in the same way?
*/



//mnumber from 1
uint64_t countIndexesInLineOnCircle(uint64_t circleNumber)
{
	return UINT64_C(2) * circleNumber - UINT64_C(1);
}



uint64_t countNumbersInNextCircl(size_t lineNumber)
{
	uint64_t value = UINT64_C(4) * countIndexesInLineOnCircle(lineNumber - 1) + UINT64_C(4);
	return value;
}


uint64_t distanceFromInputPointToCornerOfCirctForiCircleWithNumber(uint64_t circleNumber)
{
	uint64_t value = 0;
	if (circleNumber > 2)
	{
		value = (circleNumber - 1) * 2 - 1;
	}
	else if (circleNumber == 1)
	{
		value = UINT64_C(0);
	}
	else if (circleNumber == 2)
	{
		value = UINT64_C(1);
	}
	
	return value;
}

int main(int argc, const char * argv[]) {
	@autoreleasepool {
		
		uint64_t sum = 1;
		uint64_t fNSum = 1;
		uint64_t prevFValue= 1;
		
		for (uint64_t circleNum = (uint64_t)2; circleNum <= 501; circleNum++)
		{
			if(circleNum == 4)
			{
				NSLog(@"4");
			}
			uint64_t countIndexesOnLine = countIndexesInLineOnCircle(circleNum);
			
			uint64_t br = fNSum  + 1  + distanceFromInputPointToCornerOfCirctForiCircleWithNumber(circleNum);
			uint64_t bl = br + countIndexesOnLine - 1;
			uint64_t tl = bl + countIndexesOnLine - 1;
			uint64_t tr = tl + countIndexesOnLine - 1;
			
			sum += (br + bl + tl + tr);
			
			uint64_t fn = countNumbersInNextCircl(circleNum);
			
			fNSum += fn;
			prevFValue = fn;
		}
		
		NSLog(@"sum = %@", @(sum));
	}
	return 0;
}
